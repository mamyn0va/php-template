[![pipeline status](https://gitlab.com/mamyn0va/php-template/badges/master/pipeline.svg)](https://gitlab.com/mamyn0va/php-template/commits/master) [![coverage report](https://gitlab.com/mamyn0va/php-template/badges/master/coverage.svg)](https://gitlab.com/mamyn0va/php-template/commits/master)

# php-template

Template PHP project using composer and well-known PHP quality tools such as PHP_CodeSniffer, PHP Mess Detector & php-cs-fixer.
Quality tools are used at different steps of the software life cycle: in the editor, during commit with git hooks & in the CI pipeline.

## install instructions

PHP >= 7.0 is required.

Install composer:

```sh
curl -sS https://getcomposer.org/installer | php
```

Then put `composer.phar` where you want and create an alias to run it from anywhere:

```sh
alias composer='php /path/to/composer.phar'
```

```sh
git clone https://gitlab.com/mamyn0va/php-template.git
composer install
```

This will automatically create a `pre-commit` script for git in `.git/hooks` to check all your commits against the quality tools.

## editor configuration

In your preferred editor, install the plugins required by the following PHP quality tools:
 - PHP_CodeSniffer
 - PHP Mess Detector
 - php-cs-fixer

Example for Atom:

```sh
$ apm install linter-phpmd  
$ apm install linter-phpcs  
$ apm install php-cs-fixer
```

Then, configure these plugins to use the project's configuration files: `phpcs_psr2.xml.dist`, `phpmd.xml.dist` and `.php_cs.dist`.

This will allow you to have direct feedback regarding your code modifications and to allow automatic beautifying using standard coding-styles.

## CI configuration

In your CI pipeline, just add a stage for quality checks (or edit your existing `test` stage) and add a call to the PHP quality tools:

```sh
./vendor/bin/phpunit --testsuite=unit --coverage-text --colors=never --log-junit reports/phpunit-junit.log --testdox-html reports/phpunit-report.html
./vendor/bin/phpcs -sw --standard=phpcs_squiz.xml.dist src --basepath=. --report=full --report-file=reports/phpcs-report.log || echo "ok"
./vendor/bin/phpmd src html phpmd.xml.dist --reportfile reports/phpmd-report.html --ignore-violations-on-exit
```

Note:
> PHPUnit configuration will require the xdebug extension to be installed to enable the code coverage.

Example for gitlab-ci:

```yaml
image: composer

stages:
  - test
  - deploy

test:app:
  stage: test
  script:
    - apk add --no-cache $PHPIZE_DEPS
    - pecl install xdebug-2.6.0
    - docker-php-ext-enable xdebug
    - composer install
    - ./vendor/bin/phpunit --testsuite=unit --coverage-text --colors=never --log-junit reports/phpunit-junit.log --testdox-html reports/phpunit-report.html
    - ./vendor/bin/phpcs -sw --standard=phpcs_squiz.xml.dist src --basepath=. --report=full --report-file=reports/phpcs-report.log || echo "ok"
    - ./vendor/bin/phpmd src html phpmd.xml.dist --reportfile reports/phpmd-report.html --ignore-violations-on-exit
  artifacts:
    paths:
      - reports
    expire_in: 30m

pages:
  stage: deploy
  dependencies:
    - test:app
  script:
    - mkdir public
    - cp ci/pages/index.html public/
    - cp reports/* public/
  artifacts:
    paths:
      - public
  only:
    - master
```

Note:
> In this example, we publish the PHP quality tools' reports on gitlab pages so that they can be accessible outside of the job, and at anytime, on the following URL: `https://<user-or-group-name>.gitlab.io/<project-name>`
> For this project, here is the URL: https://mamyn0va.gitlab.io/php-template