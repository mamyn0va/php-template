<?php

declare(strict_types = 1);

if (!file_exists('.git/hooks')) {
    echo '.git/hooks not found, skipping git pre-commit hooks installation';
    exit(0);
}

echo 'installing git pre-commit hooks';
copy('contrib/pre-commit', '.git/hooks/pre-commit');
chmod('.git/hooks/pre-commit', 0755);
exit(0);
