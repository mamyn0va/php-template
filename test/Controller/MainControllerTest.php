<?php

declare(strict_types = 1);

require(dirname(__FILE__) . '/../../vendor/autoload.php');

use Mamyn0va\PhpTemplate\Controller\MainController;

use PHPUnit\Framework\TestCase;

class MainControllerTest extends TestCase
{
    public function testGetHome()
    {
        $controller = new MainController();
        $result = $controller->getHome();
        $this->assertEquals(null, $result);
    }
}
